#!/usr/bin/env python3

from setuptools import setup

setup(
    name="PyQtConsole",
    version="0.1",
    description="A simple but functional GUI Python console, implemented in PyQt5.",
    long_description="""A simple but functional GUI Python console, implemented in PyQt5.""",
    author="Patrick Tapping",
    author_email="mail@patricktapping.com",
    maintainer="Patrick Tapping",
    maintainer_email="mail@patricktapping.com",
    url="http://bitbucket.org/ptapping",
    platforms=[
        "Linux",
        "Windows"
    ],
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "Natural Language :: English",
        "Operating System :: Microsoft :: Windows :: Windows 7",
        "Operating System :: Microsoft :: Windows :: Windows Vista",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development :: User Interfaces",
        "Topic :: Utilities"
    ],
    packages=["PyQtConsole"],
    package_data={"PyQtConsole": ["PyQtConsole.ui"]},
    requires=["PyQt5"],
    zip_safe=False,
)
