# PyQtConsole

A simple but functional GUI Python console implemented in PyQt5.

The application may be run stand-alone, or easily incorporated into an existing application to include Python console functionality.